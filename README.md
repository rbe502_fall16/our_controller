# README #



### What is this repository for? ###
RBE 502 Group Project
Fall 2016

Amit Trivedi, Victoria Wu, Keith Ruenheck, Will Pryor

Information about meshes:
The meshes folder contains the following:
holyCube15.dae - A cube 0.5 meters on each side, with a hole centered at the top. The hole is 0.06m deep with r=0.025m.  
fatNail.dae - A pointless nail 0.1 meters long with a shaft of r=0.02m, and a head of 0.03m.

The idea is that a robot can pick up the nail and insert it into the hole in the top of the cube.

### Installation ###
You will need the turtlebot and the turtlebot arm. You can get installation instructions for turtlebot and turtlebot_arm [here](http://wiki.ros.org/turtlebot/Tutorials/indigo/Turtlebot%20Installation) and [here](http://wiki.ros.org/turtlebot_arm/Tutorials/indigo/Installation).

Our arm controller code is located another repository, our_gazebo_controllere [here] (https://bitbucket.org/rbe502_fall16/our_gazebo_controller).

The meshes need to be copied to the folder ~/.gazebo/models/my_meshes (note the period before gazebo). The file
projectCube.world places the cube at x=0, y=-2m, z=0 and the nail at x=1m and z=0.4m (on top of a pre-existing cube).


### Misc ###
Testing Gripper
IF you don't have turtlenot_teleop, 
 got tohttps://github.com/turtlebot/turtlebot and download.

`roslaunch turtlebot_teleop keyboard_teleop.launch`

`rostopic pub /pd_controller/desired_config our_gazebo_controller/arm_config -- 0 0.8 1.5 -0.8`