#!/usr/bin/env python

""" angularMotionControl.py
    Using the /odom topic to rotate a robot a given angle.
    Based off odom_out_and_back.py by Patrick Goebel
"""

import rospy
import sys
from geometry_msgs.msg import Twist, Point, Quaternion
import tf
from transform_utils import quat_to_angle, normalize_angle
from math import radians, copysign, sqrt, pow, pi

class AngularMotionControl():
    def __init__(self,argv):
        # Give the node a name
        rospy.init_node('angular_motion_control', anonymous=False)

        # Set rospy to execute a shutdown function when exiting       
        rospy.on_shutdown(self.shutdown)

        # Publisher to control the robot's speed
        self.cmd_vel = rospy.Publisher('/cmd_vel_mux/input/teleop', Twist, queue_size=5)
        
        # How fast will we update the robot's movement?
        rate = 20
        
        # Set the equivalent ROS rate variable
        r = rospy.Rate(rate)
        
        # Set the rotation speed in radians per second
        angular_speed = float(sys.argv[1])
        print('angular speed: '+str(angular_speed))
        #angular_speed = -0.1
       
        # Set the angular tolerance in degrees converted to radians
        angular_tolerance = radians(1.0)
        
        # Set the rotation angle in radians (180 degrees)
        angular_rotation = pi/int(sys.argv[2])
        print('angular rotation: '+str(angular_rotation))
        #angular_rotation = pi/4

        # Initialize the tf listener
        self.tf_listener = tf.TransformListener()
        
        # Give tf some time to fill its buffer
        rospy.sleep(2)
        
        # Set the odom frame
        self.odom_frame = '/odom'
        
        # Find out if the robot uses /base_link or /base_footprint
        try:
            self.tf_listener.waitForTransform(self.odom_frame, '/base_footprint', rospy.Time(), rospy.Duration(1.0))
            self.base_frame = '/base_footprint'
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            try:
                self.tf_listener.waitForTransform(self.odom_frame, '/base_link', rospy.Time(), rospy.Duration(1.0))
                self.base_frame = '/base_link'
            except (tf.Exception, tf.ConnectivityException, tf.LookupException):
                rospy.loginfo("Cannot find transform between /odom and /base_link or /base_footprint")
                rospy.signal_shutdown("tf Exception")  
        
        # Initialize the position variable as a Point type
        position = Point()
            
            # Initialize the movement command
        move_cmd = Twist()
            
            # Get the starting position values     
        (position, rotation) = self.get_odom()
                        
        x_start = position.x
        y_start = position.y
                       
            # Set the movement command to a rotation
        move_cmd.angular.z = angular_speed
            
            # Track the last angle measured
        last_angle = rotation
            
            # Track how far we have turned
        turn_angle = 0
            
        #self.cmd_vel.publish(move_cmd)
        #r.sleep()
        while abs(turn_angle + angular_tolerance) < abs(angular_rotation) and not rospy.is_shutdown():
                # Publish the Twist message and sleep 1 cycle         
                self.cmd_vel.publish(move_cmd)
                r.sleep()
                
                # Get the current rotation
                (position, rotation) = self.get_odom()
                
                # Compute the amount of rotation since the last loop
                delta_angle = normalize_angle(rotation - last_angle)
                
                # Add to the running total
                turn_angle += delta_angle
                last_angle = rotation
                
        rospy.sleep(1.0)
            
        # Stop the robot for good    
        angular_speed = 0
        move_cmd.angular.z = angular_Speed
        self.cmd_vel.publish(Twist())
        
    def get_odom(self):
        # Get the current transform between the odom and base frames
        try:
            (trans, rot)  = self.tf_listener.lookupTransform(self.odom_frame, self.base_frame, rospy.Time(0))
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            rospy.loginfo("TF Exception")
            return

        return (Point(*trans), quat_to_angle(Quaternion(*rot)))
        
    def shutdown(self):
        # Always stop the robot when shutting down the node.
        rospy.loginfo("Stopping the robot...")
        self.cmd_vel.publish(Twist())
        rospy.sleep(1)
 
if __name__ == '__main__':
    try:
        AngularMotionControl(sys.argv)
    except:
        rospy.loginfo("Angular Motion Terminated for the node.")

