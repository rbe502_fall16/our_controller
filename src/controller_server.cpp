#include "ros/ros.h"
#include "our_controller/moveArm.h"

double PDControlLaw()	{
	//PD Control!!!!
	return 0.0;
}


bool moveArm(our_controller::moveArm::Request  &req,
	our_controller::moveArm::Response &res)
{
  //ROS_INFO("sending back response: [%ld]", (long int)res.sum);


	//FIRST... grab the desired end effector pose fromm req	 and change to joint angles using IK
	//req.ee_pose

	//THEN... grab our CURRENT joint angles 
	
	//Calculate Error
	
	//While we are not yet at the right position.. ie error != 0
	bool atDesired = false;
	while(!atDesired)	{
	
				//We need to generate control signal and apply.
				double tau = PDControlLaw();
				//Then measure where we are currently, see if we are there yet.	
				//getCurrentJointAngles()

	}
  return true;
}




int main(int argc, char **argv)
{
  ros::init(argc, argv, "controller_service_server");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("moveArm", moveArm);
  ROS_INFO("Ready to move arm to desired EE pose.");
  ros::spin();

  return 0;
}
