**This README is for how to command the mobile base to move.**

There  are two classes that account for linear and angular motion of the robot respectively -

*linearMotionControl.py*
*angularMotionControl.py*
 
Linear motion of the robot can be configured using linear speed and distance parameters.
So to move the robot towards the peg run the following:

	rosrun our_controller linearMotionControl.py [linear speed in m/s] [linear distance in m]

For example:

	rosrun our_controller linearMotionControl.py 0.15 0.6

Angular motion of the robot is also configurable using angular velocity and angular rotation parameters. 
To turn the robot towards the hole (clockwise direction) after lifting the peg run the followng:

	rosrun our_controller angularMotionControl.py [angular velocity in rad/sec] [denominator of angle of rotation]
	
Here denominator indicates fraction of pi, so to use angle pi/6 the second argument will just be 6, such as:

	rosrun our_controller angularMotionControl.py -0.1 6

**Note-1**: 
Lower velocity values such as above do not destabilize the robot and the robot stops where intended. For rotation too, lower angular velocity limits the overshoot that happens when robot is trying to stop after it has reached the desired angle. Even then the turn angles are very sensitive, what might appear a pi/2 turn can actually be achieved by ~pi/4 value.

**Note2**: 
For clockwise turns use negative sign for angular velocity and for counter-clockwise turn use positive sign for angular velocity.